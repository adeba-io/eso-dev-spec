# Templates

These define the general layout of a function, container or mixin, and allow
you to then define an implementation on the fly.
Templated constructs take parameters for their generation, which can be declared
and used in two ways.

First, there is the in place method, which only works for functions.
You can simply prefix the first occurance of any templated type within the parameter
list with a question mark `?`.
The benefit of this method is that it is more concise.

Then, there is the up front method.
Simply place a `?` after the return type of a function, the mixin list of a data
container or the `mixin` token of a mixin, then follow with a comma separated
list of tokens and parameters.
A benefit of this method is that you can have non-type template parameters.

You can always mix the two methods.

When using a templated construct, you insert a space then `?` then a list of
inputs for the template parameters right after the name of the construct.

If the use is some sort of function call (e.g. a function or creator) and its
parameters use a template type you can omit them from the list above.
(If the rest is an empty list remove the `?` too).

With data containers and mixins, you must put the template parameter list within
parentheses `()`.

```
print(p ?T) { ... }

slist struct ? T, size u32
{
    ...
    data [size]T
}

dlist struct ? T
{
    ...
    data *[] T
}

...

jumps s32 = 8
fighter GameObject:Prefab(....)

print(jumps)   // OK
print(fighter) // OK

// Specify
print ?s32(fighter) // Error

//                   Must be a compile time constant
toDelete slist ?(u32, 4) (...)
```

## Template Specialisation

You can specialise a template for a particular type parameter, giving custom behaviour.

```
sort(l list?(?T)) { ... }
Test struct ? T
{
    print
    {
        printf('Regular')
    }
}

sort(l list?(char)) { ... }
Test struct ? s8
{
    print
    {
        printf('s8 version')
    }
}
```

## Constraints

*Syntax needs to be revised*

You can put requirements on any type being used as a template parameter called constraints.
If a template is intended to only take types that fulfill specific requires, it means
that the template needs some constraints.

```
Incrementable constraint ? T
{
    { a T; a++ } // Checks the code block is valid
}

Decrementable constraint ? T
{
    { a T; a-- }
}

IncAndDec constraint ? T
{
    is Incrementable?(T)
    is Decrementable?(T)
    not { a T; a * 4 } // Checks is code is not valid
}
```

You then use them like this.

```
list struct ? T Incrementable { ... }

print(l T) ? T Printable { ... }
```

Without constraints, the template will be resolved and throw an error where it
encounters one.
Constraints will yield far cleaner errors than this.

## `tif .. telif .. telse .. tend`

We can choose to optionally include code and members within a templated
construct.

```
Moveable constraint ? T
{
    { a T; b T =< a }
}

list struct ? T
{
    ...

    pop T
    {
        ...
        tif Moveable(T)
            return <object
        else
            return object
    }
}
```
