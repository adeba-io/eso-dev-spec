# Memory

## Allocators

An allocator allocates and deallocates memory for the program.
An allocator is simply a struct that has two specific functions.

```
SomeAllocator struct
{
    ...
    alloc(length u32, file []char, line u32) *byte;
    free(ptr *byte, file []char, line u32);
    ...
}
```

Allocators can be made use as part of the pointer assignment syntax
by placing the allocator field just after the type declaration.

```
Kart struct { ... }
alc_karts SomeAllocator(#size(Kart) * 512)

kart_slot *Kart alc_karts
// Equivalent to: kart_slot *Kart =* alc_karts.alloc(#size(Kart), __FILE_NAME__, __LINE__)

kart_empty *Kart alc_karts {}
// kart_empty *Kart =* alc_karts ... ; kart_empty{}

kart_heavy *Kart alc_karts (weight = 100)
// kart_heavy *Kart =* alc_karts ... ; kart_heavy.__create(weight = 100)

kart_light :* alc_karts = Kart(weight = 3)
kart_funky :* alc_karts =< Kart(drift = -2)

...

delete* alc_karts kart_heavy // kart_heavy.__delete(); alc_karts.free(kart_heavy*, __FILE_NAME__, __LINE__)
```

## The Default Allocator

This is the allocator used in all cases of implicit pointer allocation where an allocator is not declared.  
It takes the form of a struct with a special syntax.

```
__DEFAULT__ALLOCATOR__ struct
{
    alloc(length u32, file []char, line u32) *byte { ... }
    free(ptr *byte, file []char, line u32) { ... }
}

__ALLOCATOR__ __DEFAULT__ALLOCATOR__{}
```

If you don't declare the `__ALLOCATOR__` field, the compiler will declare it for you.
Although, you may want to if your allocator uses a creator.

If a user does not explicitly define this struct, the compiler generates one:

```
__DEFAULT__ALLOCATOR__ struct
{
    alloc(length u32, file []char, line u32) *byte { return malloc(length) }
    free(ptr *byte, file []char, line u32) { free(ptr) }
}

__ALLOCATOR__ __DEFAULT__ALLOCATOR__{}
```

## Scoped Memory Management

When making an allocation you can attach the `scope` keyword to it just after the allocator

```
arena MemArena

runSpeed *f32 scope = 12.5
runSpeed *f32 scope MemArena = 41

runSpeed *f32 scope // Error
runSpeed *f32 scope = malloc(#size(f32)) // Maybe?
```

When that variable goes out of scope, the deallocator is called on that variable

```
Run(c Character)
{
	runSpeed *f32 scope = 12.5
	...
}

// Is converted to

Run(c Character)
{
	runSpeed *f32 = 12.5

__exit:
	runSpeed.__delete // If available
	free(runSpeed)
}

// You can also specify the deletion method

//                       Name of deleter
activeOrbs *[] orb scope all = ...
```

You can name local scopes like so:

```
{:Scope // Follows the same naming rules for other identifiers

}
```

You can now use this to be more specific about which scope this variable belongs to

```
Run(c Character)
{
	// Scope is the whole function
	runSpeed *f32 scope = 12.5
	...
	{:stepper
		// Scope is also the whole function, due to the : prefix
		traction *f32 :scope = ...
		...
		// Scope is stepper
		groundFriction *f32 scope = ...
		...
		while (xyz)
		{
			// Scope is :stepper
			otherMass *mass scope:stepper = ...
			...
		}
	}
}

// With specified deleter
otherMass *mass scope:stepper fromDB = ...
```

*May want to move this scope stuff to be a prefix, with the expected style being to place
a new line after the deleter specifier*

## Bit Fields

These constrain a primitive data type to only make use a subset of its number of bits.
Only used within data containers.

For variants, we can constrain the backing type of the cases.
As a result of which, each case must have either no fields or the first field(s) must
be a primitive that uses up the rest of the space.
