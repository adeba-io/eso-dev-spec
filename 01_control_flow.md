# Control Flow

## `if` Statement

```
if canJump // No need for parantheses
{
	jump()
}
else if canRun
{
	run()
}
else
{
	stand()
}

if canJump, jump() // One line. Notice the comma

if canJump, jump() else stand()

// Can also be used as a statement

movementSpeed := if onGround, groundMoveSpeed else airMoveSpeed
movementSpeed := if onGround ,
  {
	  ... // Some calculations
	  return value // Required
  }
  else {
	  ... // Some other calculations
	  return value
  }
```

## `match` Statement

Works using pattern matching.

```
match index
{
1: // First option
	...
2: // Second option
	...
	continue; 	// Allows fall through
_:				// Default case
	...
}

match state, isReady
{
Begin, true:
	...
End, _:
	...
_, false:
	...
_:
	...
}
```

With variants, the `match` statement will throw a compiler
error when all cases aren't used.
We can use the keyword `cpick` (cherry pick) to by pass this.

## `loop` Statement

```
loop
{ ... }

break	// Exits the current loop
skip	// Skips to the next iteration of the loop

loop 5 // Loop 5 times
{ ... }

times uint8 = 45
loop times
{ ... }
```

## `while` Statement

```
loop := true
while loop
{
	...
}
finally { ... } // This block is if the loop is exited without breaking

break; skip // Same as loop
```

## `iter` Loop

```
iter n in numbers // Gets the iterator
{
	// n is the current value
}
finally { ... } // Same as in while

break; skip // Same as in loop
```

We can generate enumeratables to iterate over.

```
0..10  // 0, 1, 2, ... , 9  | Exclusive
0...10 // 0, 1, 2, ... , 10 | Inclusive

```

## Labels and the `goto` statement

As it works in other languages. Label a point in code, then call `goto` to jump immediately to it.

```
	while ...
	{
		while ...
		{
			...
			if someCondition, goto postComplexOperation
		}
	}

	...

postComplexOperation:
	...
```

You cannot goto a label outside of the statement's scope.
This is mainly intended to be used with macros.
