# Preprocessing

Preprocessing describes alterations made to the code base prior to compilation.
The order of the headers in the file represent the order in which these steps
are executed.

## Tokenisation

*I don't think this is the best name*

*Note*: These predefined preprocessor directives are case insensitive
This is a simple step of selecting code to include in the post processed output.

#### `#def` and `#undef`

Define tokens to be used as flags for use by other tokenising tags.
No need to allow them to take params like C defines as we have macros below.

#### `#run`

Prepend this to a function call to run in place at compile time.
The return of the function (if any) is pasted as is in place of the function
code in the output.
Any assignments to global variables during execution have their new values
pasted in.
Parameters to the function must be compile time constants.

```
fibonacci(i u32) u32
{
    return if i <= 1, i else self(i - 1) + self(i - 2)
}

main
{
    val := #run fibonacci(4)
    ...
}

// Goes to

main
{
    val := u32(3)
}
```

#### `#entry`

Denotes the entry point of the programming.
In the abscense of this token, the function named `main` will be assumed to be
the entrypoint.

#### `#main ... main#` Block

A block to be used only within functions.
If the function used with it is marked with the `#entry` token, two version of
it are made.
The first version will include the code within the block and will be used as the entry
point.
The second version will exclude the code within the block and will be used by the rest
of the program.

#### `#type()`

Returns a compile time object that contains information about the type passed in.

```
math codespace {
vec2 struct { ... }
}

main
{
    print(#type(vec2).fullName) // Prints math:vec2
}
```

#### `#insert`

Finds the file referenced and pastes its contents in the current file.

#### `#if ... #elif ... #else ... if#` Block

Include the code within if the expression evaluates to true.
With defines, it evaluates to true if the define exists.

## Macros

Macros provide a way to abbreviate longer code snippets.
They are somehat similar to templates but are intended for expressions.

To define one, write the `#macro` token followed by the name of the macro.
Then write the format of the macro.
For the macro parameters start with a hash, followed by parentheses.
Within the parantheses write the name of the param followed by its type.
These are the types accepted:

- INTEGER: 30, -4, 6
- DECIMAL: 0.4, -32.2, 100., .0045
- CEXPR (Compound Expression): `x, y := 4, 3.1`, `i := 0`
- EVAL (Evaluation, non-assignment expression): `x < y`, `it == end()`
- CODE_BLOCK: `singleStatement()`, `{ ... }`

After put the `#to` token, then put the code snippet to construct.
Anything within double quotes `""` will be pasted.
You can get the value of the macro params by prefixing them with hash `#`.

If the macro and its evaluation are complex enough to need multiple lines,
you can wrap them within curly braces `{}`.
For the resolution, curly braces allow you to access the params as
fields and make fields of your own.
You can access them as the raw char string. or convert them to eso types.
You will be able to do stuff like read, insert and remove statements for a
code block.

Custom allocators cannot be used in this case, and the only one provided
will have a hard limit.

Macros can generate other macros, in which case the `#evaluate` token
must prepend the `#to` block.

If you wish to use a macro for simple replacement. Write the `#macro` token,
followed by its name then a colon `:` and finally the text to paste.

```
// Example of a macro an unsigned 8-bit integer
#macro uint8
    #(num INTEGER)u8
#to
    // Double quotes is code pasted
    "u8(#num)"

// Example of a macro for float literals
#macro float32_dec
    #(num DECIMAL)f
#to
    "f32(#num)"

#macro float32_int #(num INTEGER)f #to "f32(#num)"


// Example of a macro for for loop
#macro for_loop
{
    for #(init CEXPR), #(limit EXPR bool), #(inc CEXPR)
        #(fbody CODE_BLOCK)
    finally
        #(fin CODE_BLOCK)
}
#to
{
    "{
        #init
        while #limit"

    fbody.append(inc)
    "       #fbody
        finally
            #fin
    }"
}

// Simple ID replacement
#macro PROGRAM_NAME : 'HelloWorld'

#macro asset_path
    -D#(path CHARARR)
#to
{
    safe_path := path.InsertEscapes()
    "Directory('#safe_path')"
}
```

There are some helper functions:

- PRINT, WARN, ERROR: Prints a message in the compiler output
- PUT: Pastes the passed in value in the macro output

## De-Sugaring

Syntactic sugar refers to aspects of the programming syntax that provided
to make writing and reading code more convenient, like built-in macros.
The desugared code can be spat out so it can be easier to see what the code is
actually doing.
It is also what debuggers will display when stepping through, so you can be
more accurate with your stepping.

Below goes through some sugar features and how they are resolved.

#### Inferred Types

Replaced with explicit type declarations.

#### Compound Statements

Split into individual statements, in the order they are evaluated.
The code has comments showing its source file and line number

```
// main.eso:line230
pythag := sqrt(sq(a) * sq(b))

// to

temp230_a f32 = sq(a)                      // main.eso:230
temp230_b f32 = sq(b)                      // main.eso:230
temp230_sqrt_1 f32 = temp230_a * temp230_b // main.eso:230
pythag f32 = sqrt(temp230_sqrt_1)          // main.eso:230
```

#### Forward Declarations

Definitions get hoisted to their declarations.

#### Default and Named Parameters

In all calls to the function, the call is resolved by ordering the parameters
correctly and inserting the default constant.

#### `params` Keyword

Construct an array from its inputs.

#### Containers

Creators, deleters and other member functions (copy, move) get moved to standalone
functions, that take a reference to the calling object as a first parameter.
Calls to members are made to be accessed through the calling object.

#### Anonymous Containers

They get hoisted to the closest, non-function scope and get given a name.

#### Mixins

All members associated with the mixin are prefixed with the mixin name.
All containers that use a mixin are prefixed with a function `XXXToMixinYYY`
that converts it to a mixin object.

*Need to figure out how to best resolve this*

#### Templates

Find all parameter configurations using the template, then create
copies of the templated constructs with these types filled in.

```
print(t ?T);

//////////////

print_tpl_string(t string);
print_tpl_s32(t s32);
print_tpl_vec3(t vec3);
```

## Reflection

If used, do best to resolve the values in place.
This would remove the need to store the reflection containers in the executable.
