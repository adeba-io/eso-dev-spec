# Aliasing

## Type Aliasing

Using the `alias` keyword, we can define an alias to use a type by.

```
day alias u8

mut dayOfWeek day = 6

x u8 = dayOfWeek + 1 // OK
dayOfWeek = x        // OK
```
In the above example, we can see that we can assign a regular u8 to a day
field. This is because `day` is only an alias, a u with a costume.

We can create a new, simple type using the `derive` keyword. *Keyword not set in stone*  
Within the curl braces we can set an accepatble range of values for each field.

```
// Define a range for each field
day derive u8{ 0...6 }

dayOfWeek day = 4
x u8 = dayOfWeek // OK, if the original type has a copy assignment
dayOfWeek2 day = x // Error

vec2 struct
{
    x, y, f32
    ...
}

joystick derive vec2{ -1.0 ... 1.0, -1.0 ... 1.0 }
```

## Flags

A special type alias for represent a series of booleans in a single integer.

```
FCollisionLayer flags s32
{
    // 0b1,  0b10,     0b100,   0b100000
    Ground, Water, Character, Projectile = 32
}
```

All flags have two extra fields: None (0x0) and All (Ground | Water | ...)

## Reinterpreting

There is no casting as seen in other languages.
Rather, we define functions that take an object of the source type and
return an object of the target type.

```
s32(str []char) i32 { ... }

str := '12'
count := s32(str)
// UFCS
count2 := str.s32()
```

Masking allows you to reinterpret the bytes of one object as that of a different type.
This bypasses the string type system, as such, there are compiler options to throw
errors when this is encountered.

```
asciiZero u8 = 0x30
chrZero := (char)asciiZero

// charZero == '0'
```

## Unions

Unions enable you to store fields of different data types in the same location.
The size of a union will always be the size of its largest member.

```
data union
{
    as_byte *byte
    as_s32 *s32
    as_chars *[4]char
}

dat data

dat.as_byte =* 0x854f21bc
```

The size of the `data` union will be 8 (on 64-bit hardware).

We can define unions to have some compound members, which are laid out sequentially
to each other.
They are defined between another set of curly braces within a union. Their first element
still starts in the same location as the other elements of the union.

```
event struct
{
    name []char
    type u8

    union
    {
        position vec2
        offset vec2
        { // These are laid sequentially
            key Key
            state ControlState 
        }
        { // So are these
            button Button
            state ControlState
        }
    }
}
```

## Field Aliasing

We could give a field multiple names using unions, but field aliasing gives us a much more
concise syntax.

```
// Rather than
float3 struct
{
    union
    {
        { x, y, z f32 }
        { r, g, b f32 }
        { s, t, u f32 }
    }
}

// Use
float3 struct
{
    x | r | s f32
    y | g | t f32
    z | b | u f32
}
```
