# Eso Dev Spec

This repo houses the (work in progress) development guidelines for the esoteric
programming language, eso (working title) that I am creating.  

---

This is my attempt at making a low-level, C-like language primarily to be used for games.

## TODO

Steal a bunch of stuff from Odin
- or_else, or_return, etc
- enumerated array
- assign to array by range of initialisers
- array programming?
- aos switch
- array slices
- bit fields
- range syntax?

Figure out which ones are for libraries or syntax

## Overview

### Goals

- Speed
- Low Level, yet to borrow some features from higher level languages
- Granularity
- Avoidance of null and Exceptions

### Philosophy

I tend to hear the phrase something along the line of "programmer time is more expensive than program time", and I agree. However, I think it's become a crutch for programmers to get away with increasingly unoptimised code. More often than not, the actual problem isn't an issue of time, rather of skill. Why else would up and coming languages like Rust, solely pride themselves on being damn near impossible to write error prone code in?

Perhaps I'm just na�ve, but I think this is an incredibly unsustainable way of thinking, from a business stand point, maybe not, but for someone that doesn't think a 2020 CPU should be necessary to run a game that could run on a Gamecube, most definetly.

### Only what I need from Object-Oriented Design

Most of the game development I have done has been in using Unity and Godot, which make heavy use of object oriented designs. However, as my coding style has developed I began to realise that I really don't need the majority of the object-oriented design decisions. Some things, such as forcing everything into a class has come across as misguided to me.
Really, the only thing I seek to borrow from the paradigm, would be interfaces, struct polymorphism and encapsulation. 

Encapsulation will mostly be used for the programmer to express intention, compiler options will exist for the user to suppress related errors.

