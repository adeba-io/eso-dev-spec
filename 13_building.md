# Building

Two building tools are provided.

1. compiler - takes a file, preprocesses it then compiles it an obj, or, if self contained, an exe
2. builder - Compiles all files in the project.

## Compiler

Files can tell the compiler that they depend on the symbols and containers from other files
using the `use` keyword.
It is followed by a path to file we want to point to.
A good practice would be to have a file that defines the codespace configuration
and use it in every file.
When using the compiler, we need to compile each file individually and then link them.

The compiler takes various inputs telling it how and what to compile the code to.

## Builder

The builder will hold configuration for the different projects to code base can compiled
to.
These projects contain information on what files/codespaces to include and what
compiler options to use.
