# Errors

Errors are handled in a return as values fashion.

First, to be allowed to create errors through this method a function
must be explicitly marked as error generating.
This function can then write to a thread local field before returning
then, the calling function can read that error.
The calling function is not forced to read/handle that error.

```
list struct ? T
{
    ...
    mut err pop <T { ... }
    err back *T { ... }
    ...
}
```

These functions can throw and write to the error using the `fallback`
keyword.
The first value is an object to be returned by the function,
the second is an 32 bit integer to act as an error code,
the third item is a string as the error message, and the final one
is an object to put in a 32 byte buffer.

```
mut err pop <T
{
    if _count == 0
    {
        fallback T{}, 0x41, "List is empty"
        // No need for return
    }

    _count--
    return <_data[_count]
}
```

A potential implementation of the error container would be:

```
Error variant
{
    Safe,
    Bad{ code u32; message string; file []char;
         line u32; data [32]byte; trace CallStack }
}
```

The calling function can read from this error using the `error` field.

```
next := projectilePool.pop()
print(error.message)
```

Note, this field is cleared when the scope the function was called in
exits.
The `error` field is also cleared when it is localised using a move or
a getter ending in an exclamation mark `!` is used.

```
errLocal :=< error
errLocal := error // Error, the error has no copy constructor

log:FilePrint(error.trace!)
```

Of course, before using the error we first need to check if the error
is in the `Bad` case.
Rather than querying with an if statement we can use an error block.

```
next := projectilePool.pop()
error {
    // This code only executes if error is Bad
    next = Instantiate<Projectile>(projectilePrefab)
} // error is cleared here
```

If we so chose, we can opt to propogate an error to function lower in
the call stack, rather than deal with it just after the call.
A function that does this must also be marked `err`.
We can do this using the `propogate` keyword or the call postfix `?` operator.
*These could be implemented as macros*

```
err throwHadouken bool
{
    ...
    next := projectilePool.pop()
    propogate false // === fallback false, error.code, error.message, error. ...
    ...
}

// or

err throwHadouken bool
{
    ...
    next := projectilePool.pop() ? false
    // next := projectilePool.pop(); error { fallback false, error.code .... }
    ...
}
```
