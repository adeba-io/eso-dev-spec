# Mixins

Mixins allow functionality to be shared amongst different containers.

```
MPaddle mixin
{
	speed f32

	Reset(); // Has to be defined by any includers 

	OnBallCollision(ball MBall)
	{
		// Default implementation
	}
}

FastPaddle struct MPaddle
{
	// Redefine all of the fields and functions from the mixins
	speed f32

	Reset { ... }

	// Use '->' to tell it to use the mixins implementation
	MPaddle -> OnBallCollision(ball MBall)
}

StancePaddle variant MPaddle
{
	Water
	{
		OnBallCollision(ball MBall) { ... }
	},
	Wind { MPaddle -> OnBallCollision(ball MBall) }

	// Mixin variables must be variant-wide
	speed f32
}
```

You can specify the mixin member you are defining by prefixing the name with
the mixin name and `:`.

```
HeavyPaddle struct MPaddle
{
	MPaddle:speed f32

	MPaddle -> MPaddle:OnBallCollision(ball MBall)
}
```

## Mixin Member Clashes

If a struct implements two mixins that have identical members - in both name and signature -
a name clash will occur.

During struct definition, start by comma listing the relevant mixins followed by `:`.

```
FastPaddle struct MPaddle, MBallCatcher
{
	MPaddle, MBallCatcher:speed f32

	MPaddle, MBallCatcher:OnBallCollision(ball MBall) { ... }
}
```

When operating on an object, you can specify by prefixing the function name with
the mixin name and `:`.
*May want to omit this and just force specifying*
If this is omitted, the version of the function called will be the version that is declared first
in the container's definition.

```
HoldingPaddle struct MPaddle, MBallCatcher
{
	MPaddle, MBallCatcher:speed f32

	MBallCatcher:OnBallCollision(ball MBall);
	MPaddle:OnBallCollision(ball MBall);
}

paddle HoldingPaddle()

paddle.OnBallCollision(ball) // Calls the MBallCatcher one
paddle.MPaddle:OnBallCollision(ball) // Calls MPaddle's one
```

## Polymorphism

You can cast a container to its mixin. This returns an object that contains
pointers to the mixin's variables and functions. No plans for you to be 
able to try to cast back.

```
fPaddle FastPaddle()

paddle MPaddle(fPaddle)
```

## Anonymous Mixin Implementation

For use when you need to create an on-the-spot implementation of a mixin.

```
paddle2 MPaddle {
	// Implement any outstanding functions here
}

paddleBall MPaddle{ ... }, MBall{ maxSpeed f32 = 22 }
```

You can create new functions in an anonymous mixin.

## How `partial` works with Mixins

Each partial container define the relevent mixins and can only use the mixins defined
in that part.
The complete container will have all the mixins.

## Mixin Creators and Deletors

*I don't think this is necessary*

If you want to perform some initialisation/deinitialisation function as
part of a Mixin.
Use case, in Unity C#, MonoBehaviour shouldn't have a constructor as MonoBehaviour does
some stuff that it needs to be done first.

```
MPaddle mixin
{
	__creator( ... ) { ... }
}

MBallCatcher mixin
{
	__creator( ... ) { ... }
}

////////

FastPaddle struct MPaddle, MBallCatcher
{
	__creator( ... ), // A
	{ ... }, // B


	__creator( ... ), MPaddle( ... )
	{ ... }, MBallCatcher( ... )
}
```

In position `A`, list the mixin creators/deletors in order to be called before the
struct's creator/deletor. You can only pass in compile time constants and variables
directly from the struct's creator.

In position `B`, list the mixin creators/deletors in order to be called after the struct's
creator/deletor. Variables evaluated in the struct's creator can be passed into the mixins'
creators.

The order you list the creators/deletors is the order they are called.
