# Functions

## Declaration

Functions are declared in a post-type notation.

```
print(str string) void []
{ ... }

sum(a f32, b f32) f32 [sumCount]
{
  sumCount++
  // Explicit return
  return a + b
}

// Call
sum(1, 32.4)
```

In the above example,

- the first element is the **Function Name**: `sum`
- the second element is the **Parameters** within parentheses: `(a f32, b f32)`
- the third element following the is the **Return Type**: `f32`
- the fourth element within square brackets are the **Captures**, the fields of the outer scope the function can access: `[sumCount]`
- the final element is the **Function Body** enclosed within curly braces: `{ ... }`

You can omit elements 3 or 4, if they are empty or take the default assumed value

- Return Type: is void: `void`
- Captures: the list is empty, meaning none: `[]`

*NOTE* should make these defaults configurable in the compiler, e.g.: `[*]` means capture everything *This may be a bad idea* *Correction: This is a bad idea*
*NOTE* Removed the ability to omit empty parentheses, with just the name and return type it is ambiguous whether or not it is a field or function

Functions can be declared and then defined elsewhere in the program. This allows for functions that could be declared within a library and then implemented in it's user's program.

```
// Declaration, you cannot omit the parenthesis during declaration only
// In this case, the semicolon is required
foo();
func2() char[];

// We can omit the names of the parameters, should only do this if the param's purpose is obvious
abs(f32) f32;
insertString(string, position u32);

...

// Definition
func void { ... }
func2 char[] { ... }

abs(value f32) f32 { ... }
insertString(s string, position u32) { ... }

// If we don't need a parameter in the definition, we can omit its name and prefix the type with a :
insertString(s string, :u32) { ... }
```

## Named Returns

You can name the return types of a function as if they were fields. The token used for that field
is defined in the scope of the function body.
When return is called or the function definition ends, the values of these return fields are automatically
returned.

```
FindFirstNonNegative(arr [] s8) result:s8
{
  result = s8{}
  for i in arr
  {
    if i >= 0
    {
      result = i
      break
    }
  }
} 
```

*This may make it difficult to see what the function is doing if the return names are not made obvious*  
*Probably a bad idea for long functions*
*Regular returns make it easier to see what data is being returned*

*The above issues are better solved through a sensible naming convention*

## Function Overloading

Functions can be overloaded only if they use different parameters, but not if they only have a
different return type.

```
sum(a s32, b s32) s32 { ... }
sum(a s32, b f32) s32 { ... }
```

You can define standalone functions, and group them together as overloads for a single definition.

```
sumInts    (s32, s32) s32;
sumIntFloat(s32, f32) s32;

sum multifn sumInts, sumIntFloat;
```

## Defer Statement

The defer statement allows you to defer the execution of a function until the end of a function's execution.

```
helloWorld
{
  defer { print('End') }
  print('Hello World')
}

//   Prints
// Hello World
// End
```

By default, `defer` calls the functions in a LIFO format.

```
helloWorld
{
  defer { print('End 2') }
  defer { print('End 1') }

  print('Hello World')
}

// Prints
// Hello World
// End 1
// End 2
```

## The Function Syntax Progression

Stolen from [Jai](https://github.com/BSVino/JaiPrimer/wiki).

```
                    { ... }    // Code Block
                 [] { ... }    // Captured Code Block
    (i s32) void [] { ... }    // Anonymous Function
func(i s32) void [] { ... }    // Named Local/Global Function
```

## Function Type

The type of a function is just its parameters and return type, elements 2 and 3 in the declaration section.

```
//           Can be omitted if void
(*char, u32) s32


//                  Get the reference by just using the name
onLand (f32, f32) = OnPlayerLand
```

## Default Parameters

Must be compile time constants or readonly fields.

```
PlaySound(audio_asset, looping bool = true);

f32 volume = 5.0
PlaySound(audio_asset, looping bool = true, scale f32 = volume);
```

## `params` Keyword

Like in C#.

```
// Declaration
addAll(params nums [] s32) s32

// Use
addAll()
addAll(1)
addAll(1, 2, 3)
```

Takes a list of parameters and collapses them into an array.  

Must be the last parameter in a function defintion. Can only be one in a function.

## Uniform Function Call Syntax

Applies to all non-member functions.
Any function can be called using the syntax of a member function call.

```
print(str string) { ... }

// Can be called with
greeting := 'Hello! My name is John.'

print(greeting)
greeting.print()
```

## Function Chaining

You can call functions on the same field in succession, without having to type out the field name again
using a comma `,`.

```
abs!(f32*); // Changes the field to its absolute value
sign(f32) i8; // Returns the sign of the field

x := -243.452

// Rather than
x.abs!()
s := x.sign()

// We have
s := x.abs!(), sign()
```

## Polymorphic Function Definitions

Like in functional languages.

```
processCollision(type EColType, self ColBody, other ColBody, velocity vec2);

poly processCollision(EColType:Enter, self ColBody, other ColBody, velocity vec2) { ... }
poly processCollision(EColType:Exit, self ColBody, ColBody, vec2) { ... }
poly processCollision(type EColType, self ColBody, other ColBody, velocity vec2) { ... }
```

You can set up custom resolution definitions.

## Currying

As seen in Haskell.  
To curry a function, fill in the parameters you know and end the parameters with a comma `,`.

```
// Base function
processCollision(type EtyCol, self ColBody, other ColBody, velocity vec2);

// Define a new function with it
processEntryCollision = processCollision(ETypeCol:Enter, ) // Params resolved in order
processMyCollision    = processCollision(, _myBody, ) // The first comma skips the first param

// Define a new function out of order (with named parameters)
processNoVelCollision = processCollision(velocity = vec2(0, 0), )

// Curry an operator
double := *(2, ) // double(x i32) i32 { return x * 2; }
c := double(5) // 10
```

## self keyword

A function can call itself using the keyword `self`.

```
factorial(x u32) u32
{
  if x == 1, return x
  return self(x - 1)
}
```

Best used with anonymous functions or ones with really long names.
If the function is overloaded, self always calls the version it is in.

## pure keyword

Use to mark a function as one that doesn't manipulate any state other than the ones
it creates.  
*Isn't this just empty captures?* <- *Yes*

## Modifiers

We have function modifiers to tell the compiler how this function should be handled.  
These are placed between a function's captures and their function body, therefore
we can only place these on a function's definition.

**inline**:  
Rather than a call instruction, past the contents of the function at the call site
We can also place this at the call site of the function, to inline it there.

**tailrec**:  
If a function is recursive and returns a call to itself as its last operation, it is
optimised into a loop.

## Parameter Modifiers

**out**:  
This would work by passing a pointer into the function and then writing to it as am output.
This is not necessary.
Instead we have the "move" return-type syntax `<T` (only valid for return types).
This is syntactic sugar over the above, so if you construct the returned object in the
return statement no the mover will not be called.

## Trailing Lambdas

Stolen from Kotlin. Not sure if it's necessary.
