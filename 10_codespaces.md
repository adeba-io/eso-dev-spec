# Codespaces

## Name Prefixes

Codespaces primarily act as name prefixes for a collection of tokens.
The tokens in codespaces are accessed relatively, using the `:` operator.
We can access the tokens starting from the global codespaces by prefixing
the token from `::`.

```
io codespace
{
    istream struct { ... }

    read char { ... }
}

c := io:read()

print codespace
{
    log(s []char) { ... } // 1
}

game codespace
{
    print codespace
    {
        log(s []char) { ... } // 2
    }

    SomeFunc
    {
        print:log('Calls 2')
        ::print:log('Calls 1')
    }
}

// B:A:print could be defined like so
game:print codespace
{
    log(s []char) { ... }
}

DB codespace
{
    variant { SQL, NoSQl, MongoDB, PostgreSQL }
}

dbType DB:variant = DB:PostgreSQL
```

Note that `::print` is not the same codespace as `::game:print`.
The parts of a partial container must be defined within the same codespace.

## Codespaces as Scopes

You can define a default allocator within a codespace.
It will be used as the default allocator for all default memory allocations in that
codespace.  
This works because the use of the global allocator is not prefixed with `::`, so it will
always use the one from the closest codespace parent.

## Configuring Code

You can configure codespaces to compile the code within them using certain settings.

When used, codespaces act as hard separations between sections of the code base.
They must explicitly declare what other codespaces they can access.
They can define macros and preprocessor values.
They can be configured to ignore the `pri` keyword from other codespaces.
They can be configured to replace tokens found in other codespaces with code snippets.
etc.

```
debug codespace:
    INCLUDE [ core, math ]
    IGNORE_ENCAPSULATION
    DEFINE [ 'DEBUG' ]
{
    ...
}
```

*Syntax still needs to be figured out*
*Should we include `namespace` as a keyword for codespaces that lack this configuration?*

## `using` Keyword

Use the `using` keyword to include all the symbols from the codespace at the local level.

```
// Includes them all for this file
using std

somefunc
{
    // Includes for this function
    using std
}
```
