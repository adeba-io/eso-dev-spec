# Operators

Operators desugar down to functions.
Should make it possible to define your own operators, but not
sure how I would define the more primitive ones.

Declarations start with the `operator` keyword, then the format wherein the operator is used.
The parameters for the operator are wrapped in parentheses and placed in the appropriate position

```
operator (a s32) + (b s32) s32
{ ... }

// User defined types
operator (a vec2) + (b vec2) vec2
{ return vec2{ a.x + b.x, a.y + b.y  } }

operator -(v vec2) vec2 { return vec2{ -a.x, -b.y } }

// These can also be called like so
+(1, -40)
+(vec2(), vec2{90, 34})
```

The operators can be any symbol that doesn't already have a specific purpose, e.g.:
`{}`, `=`, `:=`, `//`, `:`, `;`, etc.

The compiler should throw an error if the operator would cause ambiguities.

Operators can access private properties if defined with the body of the struct.

## Logic

Considering flipping the functionality of `&& and &` and `|| and |` simply because
we (I) use the boolean operations much more frequently and this would save on typing.
However, the save is really negligble and, in all likelihood, not worth the the countless
amount of errors muscle memory will cause.  
