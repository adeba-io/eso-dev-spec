# Data Containers

There are two types of data containers: `struct` and `variant`.  
A `struct` holds all of its member fields sequentially.
A `variant` has multiple states it can hold, each with their own member fields.

## Declaration and Definition

Data containers are declared in a post type notation.

```
vertex struct { ... }

collision_state variant { ... }

vec2 struct
{
	x, y f32

	// You do not need to include the struct fields as captures
	normalise
	{ ... }

	// Prefix member functions that change the object's state with mut
	// They can then only be called when the object's field is marked mut
	mut normalise! vec2
	{ ... }

	// Forward declare
	perpendicular() vec2;
	mut reflect!();
}

axis_button variant
{
	// There are the variant's cases
	RELEASED,
	DOWN      { strength f32 },
	HELD      { strength f32 },
    // Specify the backing value
	UP        { strength f32 }  = 7// Note the last one lacks a comma

	// Shared members
	buttonID u8

	mut next();
	remove_deadzone() f32;
}

...

vec2::perpendicular vec2 { ... }
// Don't forget the mut
mut vec2::reflect! { ... }

mut axis_button::next { ... }

// Polymorphic Function Definition for a variant
// Specify the case by following the variant name with ':[CASELIST]'
poly axis_button:DOWN, RELEASED::remove_deadzone f32 { ... }
poly axis_button:UP::remove_deadzone f32 { ... }
poly axis_button::remove_deadzone f32 { ... }

...

// A variant can be declared like so
btn_accelerator axis_button:HELD{ strength = 0.5 }

btn_shield axis_button // Assumes the default (first) case

// Retrieve the backing value
// s8 / u8 / s16 / ...
s32(btn_shield)
btn_shield.s32 // UFCS
```

A container's member fields must explicitly define the type.

All of a container's members can be accessed using `.`: `position.x`.

The size of a variant will be the size of the shared fields plus the size of the largest
case.

We can check the current type of a variant using the control flow statements.

```
//                Gets an immutable pointer to the strength field
if btn_accelerator:DOWN(strength) { ... }

match btn_shield
{
// No need to specify the variant
:DOWN(strength):
	...
//   Rename the pointer
:HELD(strength: str):
	...
// Get a mutable pointer, only if the field in match is mutable
:UP(mut strength):
	...
:RELEASED:
	...
}

while btn_attack:HELD(strength) { ... }
```

## Creator and Deleter

Creators are called upon the creation of a field. Deleters are called upon the deletion, by calling the `delete` keyword or by going out of scope.

```
Scene struct
{
	name string
	warrior *Character

	__create { ... } // Default creator

	__create(name string, warrior *Character)
	{
		this.name = name		// this keyword
		this.warrior = warrior
	}

	__delete
	{
		free(warrior) // Free up the resources
	}
}

GameObject variant
{
	Active
	{
		enabled bool

		// A case creator
		__create(obj GameObject:Prefab) { ... }
	},
	Prefab
	{
		assetPath []char
		data resource

		// A case deleter
		__delete { ... }
	}

	name []char

	// A variant creater
	__create
	{
		// Access the case being created with `this`
	}

	__create([]char);

	// A variant deleter
	__delete;
}

poly GameObject:Active::__create(name []char) { ... }
poly GameObject:Prefab::__create(path []char) { ... }

ScoreKeeper struct
{
	scores [2]u32

	// Notice no creator

	__create(player1 u32, player2 u32) { ... }

	// Notice no deleter
}

...

level1 Scene() // Default creator
level2 Scene('Abandoned Temple', war_Marth*) // Parametered creator

scores  ScoreKeeper(14, 12)
scores2 ScoreKeeper() // Error, as no default creator was made

levelHeap :*= Scene() // levelHeap Scene = malloc ...; levelHeap.__create()
scoresHeap :*= ScoreKeeper(0, 0) // scoresHeap ScoreKeeper = malloc ...; scoresHeap.__create(0, 0)

////////

p_hadouken GameObject:Prefab('Projectiles/hadouken.prfb')

go_hadouken GameObject:Active(p_hadouken)

...

delete level1    // level1.__delete()
delete levelHeap // levelHeap.__delete()

delete* level1    // Error
delete* levelHeap // levelHeap.__delete(); free(levelHeap*)

delete scores // Error

delete p_hadouken // Uses the deleter in GameObject:Prefab

randomFunc
{
	scene :*= Scene('Dragon\'s Roost', war_Ryu*)
	defer delete* scene
	....
}
```

They are declared similarly to functions, but cannot have a return type. `__delete` also cannot take parameters.
Thus, both can omit the same things that functions can.

In variants, case creators must have unique signatures to all variant creators.
When deleting, the current case's deleter is called first, followed by the variant deleter. 

You can call the creators and deleters manually.

## Named Creators and Deleters

Assigning creators and deleters names allows you to specify which creator or deleter you would like to use.
It also allows for multiple deleters.

```
projectile struct
{
	graphic *ParticleEffect
	speed f32
	behaviour EProjectileBehaviour

	__delete { free(graphic) }
}

projectile_list struct
{
	_elements *[] projectile

	__create     (capacity u32) { ... } // Just allocates the memory for `_elements`
	__create def (capacity u32) { ... } // Allocates and default inits all slots

	__delete     { ... } // Only frees the memory behing `_elements`
	__delete all { ... } // Calls the deleter on all elements then frees
}

projectiles projectile_list(20) // Default creator
projectiles projectile_list def(20) // Named creator `def`

...

delete projectiles all // Named deleter
```

## Variant Case Switching

You can switch the case of a mutable variant object with the following:

```
mut go_sonicBoom = GameObject:Prefab('Projectiles/Sonic Boom.prfb')
go_sonicBoom.Active(go_sonicBoom)
```

First, put the field name, followed by the `.` operator, then the name of the new case.
After that you can use the standard object initialisation methods (none, `{}` and `()`).

This calls the deleter of the previous case first, if present.
You can specify which deleter and which creator to use like so:

```
//               Deleter:Creator
go_sonicBoom.Active deep:zero_pos(go_sonicBoom)
```

## Pointer Ownership

A way to indicate pointers that should be deleted at the end of the container's deleter. Only if the 
container has a deleter.
If a container has owned pointers but no deleter an error is thrown.

```
Scene struct
{
	name string
	warrior *!Character // Note the exclamation point

	__create { ... } // Default creator

	__create(name string, warrior *Character)
	{
		this.name = name
		this.warrior = warrior
	}

	__delete
	{
		print("Deleted Scene \name")
		// warrior gets automatically deleted here
	}
}
```

The pointers owned by specific cases in variants require deleters for that specific case.

## Anonymous Containers

For structs, these would be known as tuples in other lanugages.

```
position {f32, f32} = {1.0, 10}
position.a // Access the first element

{x f32, y f32} // This is a named declaration

position {x f32, y f32} = {1, 10.0} // Named declaration and initialisation
// Named decalaration with interpretted types
position := {x f32 = 1, y f32 = 10}
position := {x = 1.0, y = 10.0}
```

You can use these to return multiple fields from a function.

```
FirstAvailableBit(bitset *[]byte) {iByte u32, iSlot u8} { ... }
```

They can also be used as a one use container for some fields.

```
struct {
	width, height u32
	screenID u8
} windowData

...

windowData.width
```

You can use it make an inplace enum, accessed within a scope without the need of a prefix.

```
variant { SQL, NoSQL, MongoDB }

...

// The type is referenced like so
dbType :variant = SQL
```

## Static Members

Global/Namespace scope fields and functions that use the struct name as a namespace/identifier.

```
// Integrated(?) Declaration
mat2x2 struct
{
	...
	static identity mat2x2
	static dotProduct(a mat2x2, b mat2x2) mat2x2 []
	{ ... }
	...
}

// Individual Declaration, no need to forward declare
mat2x2:identity mat2x2
mat2x2:dotProduct(a mat2x2, b mat2x2) mat2x2 []
{ ... }

// External Declaration, no need to forward declare
static mat2x2 struct
{
	identity mat2x2
	dotProduct(a mat2x2, b mat2x2) mat2x2 []
	{ ... }
}

...

// Usage

viewProj := mat2x2:dotProduct(view, projection)
// UFCS
viewProj := view.mat2x2:dotProduct(projection)
```


## `partial` Container

*NOTE* Will only include when it becomes necessary
This allows you to split the definition of a container up into separate files.

For variants, each part can define some cases. The final order of the cases - by default -
will be resolved by the order the parts are resolved by the compiler.

You can enforce an order for the parts by putting an order number 1-100 in parentheses after `partial`,
smallest first.

```
// File A
Controller partial struct
{ ... }

GameObject partial(3) variant
{
	Active { ... }

	...
}

// File B
Controller partial struct
{ ... }

GameObject partial(1) variant
{
	Prefab { ... }

	...
}

...

// Resolved
GameObject variant
{
	Prefab { ... }, Active { ... }
}
```

## Properties and Encapsulation

Properties enable you to either block the reading or writing to fields and/or
include some additional functionality to the read or write.  
These should always be very small so a compiler warning will be given if the method is 
doing too much.

```
whatever struct
{
	x f32,
		get, // default behaviour
		set
		{
			log("x has been set to {value}")
			x = value
		}
	
	y i32,
		get
		{
			return y * 2
		},
		set // default behaviour

	// If you put one, you must put the other
}
```

Through encapsulation, you can restrict the access of member fields and member functions.

- `pub`: The default, free access for anyone to read and write
- `pri`: Restricts access to only member functions

You can specify access for reading or writing:

- `x s32`: public read and write
  - *verbose* `pub x s32, pub get, pub set`
- `x s32, get, pri set`: public read, private write
- `x s32, pri get, set`: private read, public write
- `pri x s32`: private read and write

Whether or not violation of this should throw an error, can be 
toggled with a compiler option.


## Data Copy and Move

A special subset of creators for copy and move creating and assignment.

```
resource struct
{
	// With a semi colon, we have a default copier and mover
	// The copy copies the fields from the other object
	// The mover does the same then clears the fields from the other one
	// To enable a struct to be copied or moved at least this must be explicitly declared
	// If any of the fields in the struct are not copyable or movable respectively,
	// this declaration will through an error
	__copy;
	__move;

	data *byte
	path *char
}

resource_explicit struct
{
	// Explicitly defined copier and mover
	// other is implicitly defined to point to the other object
	__copy
	{
		data = other.data
		path = other.path
	}
	// Remember to clear it at the end
	__move
	{
		data = other.data
		path = other.path

		other = resource{}
	}

	data *byte
	path *char
}

unique struct
{
	field *char
	... // No copy nor move
}

data * byte = ....
r resource = { data, 'Assets/yadayada.asset' }

rCopied resource = r
rCopied2 := r
rMoved resource =< r
rMoved2 :=< r

copiedResourceTaker(r)
movedResourceTaker(<r) // Moves it to the parameter

rCopyCreated resource(r)
rMoveCreated resource(<r)

u unique = { "0xDEADBEEF", ... }

u1 := u // Error
u2 :=< u // Error
```

Note that the copy is just the regular assignment operator and lacks an operator for copy passing a parameter.

Functions that return a moved object need the return type to be marked specially.
These functions also need to be marked as mutable.

```
mut list::pop <T { ... }
```

*This syntax will need to be revised*

## Struct of Arrays switch

You can have an array of data containers be represented a data container of arrays using an SoA switch.
Doing this just for every field can be naive, so you can just to organise the fields of a container into
SoA blocks.
They will act as mini structs that the struct of arrays will use as the type for the arrays.
You can also restrict functions to only use member fields within that specific SoA block.

```
vec3 struct { x, y, z f32 }

regular [8] vec3
soa [soa:8] vec3
// We cannot use the soa switch on the outer array here
aosoa [8][soa:8]vec3

// TODO: Come up with an example of SoA blocks
```
