# Special Tokens

## The `ARGS` Object

A global struct that houses the input variables from the command line.
By default, it's just an array of strings being the inputs from the command line.

You can define the struct to be more complex.

*We'll see how this goes*

```
ARGS
{
    size int32;
    targetDir Directory;

    __create(commands string[])
    {
        // Must set all of the fields
    }
}
```

This gets called at the start of the program.
Will be shown in the entry point function in the de-sugared/preprocessed code.

## The Exit Code

A global variable. `__exit_code int32`

## Singletons

***Recent revelations to me in good programming practices deem this feature obsolete***

*May want to make this functionality more generic so it can be more easily used in other contexts*

You can define a singleton with the singleton keyword. All variables and functions are static.

```
Time singleton
{
    ...

    // Only one creator and destructor (maybe)
    __create() { ... }
    __delete { ... }

    deltaTime f32, pri set

    Update()
}

// Invoked like this
Time:deltaTime
Time:Update
```

Whenever a function or struct makes use of a singleton, an extra member variable/parameter will be created.
These by default are initialised to point to the instance of the singleton, if they are to be overriden they
will need to be named.
This is meant for easier testing. Could create a compiler flag that removes this functionality to save on memory.
