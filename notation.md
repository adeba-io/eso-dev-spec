# Notation Notes

## Semicolon, ;

Right now, the semicolon `;` is used to denote the end of a statement.  
Considering dropping this.

Instead, use the end of the line to denote the end of a statement.
If you want to have multiple statements on a line, break them apart with a semicolon.
Don't want to force the use of a character, e.g. `\\`, to allow multi line statements.
Rather develop a system that can detect this intelligently.

## Modifiers

`static`, `pri`, etc, etc, excluding type names.
Should they come before or after the variable or type name?
I chose post type notation so that tokens could remain inline.
However one would want to keep static tokens inline, and perhaps the same could be said for private tokens as well.
This may be the reason why Rust and Zig still use `fn` to denote functions. *The actual reason is it makes it easier to parse*

## Allowed characters for tokens

All letters, upper and lowercase, and the character `_` can be used anywhere.
These special characters: `? !` and numbers can be used but can't be the first character.

*Issue*: Template parameters are specified using `? template params...`.
For a function this would look like `funcName ? template params (func params) { ... }`.
What if the function use a trailing `?`?
`funcName? ? tparams ....` is pretty ugly.
The solutions are:
- removing `?` as an identifier token
- enforcing the template param `?` to be prefixed with whitespace
- making a new syntax for template params

## String and Char Literals

Both use single quotes `'` for literals. Chars will just take the first character
of the sequence as a value. If a character sequence longer than one is assigned to a
char there will be a warning but not an error.  
Double quotes `"` are not used, if used it will create a compiler error; unless it is used in a macro
