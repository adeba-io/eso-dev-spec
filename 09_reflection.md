# Reflection

## Reading Types

You can access an object that holds details about containers and mixins using
the `#type()` macro.

```
tController type = #type(Controller)
tMxComponent := #type(MComponent)

tController.name
tController.fullName
tController.size
tController.mixins
etc.

tMxComponent.implementers
```

Alongside stuff like this, we can also access certain traits about a type.
This can then be used for things like template constraints.
In this case, we can access the type information with just the `.` operator.

```
tController.isMoveable

Moveable constraint ? T
{
    = T.isMoveable
}
```

## Attributes

Attributes allow you to attach metadata to fields, functions, containers and
mixins.
Attributes assigned to mixins are inherited by everything that implements
it.

An attribute starts by specifying its applicable contexts.
They are listed the same way variants are; a comma separated list wherein
the last one lacks a comma.
The options are `FIELD`, `FUNCTION`, `STRUCT`, `VARIANT` and `MIXIN`.

```
Serialisable attr { STRUCT, VARIANT }
SerilaiseField attr { FIELD }

RequireComponent attr
{
    STRUCT, VARIANT

    __create(components []type) { requires = components }

    requires []type
}

Command attr
{
    FUNCTION

    __create(names [][]char) { this.names = names }

    names [][]char
}
```

When using attributes, prefix them with the `@` symbol.  
When assigning attributes they go at the end of everything in the token's
declaration i.e. before the assignment of a field, and the body of a function,
container or mixin.

```
CharacterController struct Component
    @RequireComponent(Rigidbody) @Serialisable
{
    runSpeed f32 @SerialiseField
    jumpPower f32 @SerialiseField = 9

    jump() @Command([ "j", "jump" ]);
}
```

Attributes can be uniquely instantiated like so:

```
requireComponent @RequireComponent()
```
