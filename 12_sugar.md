# Sugar

// TODO: Show how variants are constructed from structs and unions

Sugar refers to syntactic sugar. Stuff that won't be in the assembly code but exist to
make our lives easier.

## The Preprocess/De-Sugaring Step

This is the first phase of compilation that resolves all macros, generics,
preprocessors, etc, to create much lower level code. This is the code the
interpreter/debugger runs and users can inspect to give them greater detail
of what their code is doing.

In the output file, special comments will link back to the file and line
the statement is from.

## Sugar Features and How They're Resolved

#### Generics

Go through the code base and find all types that are used with the 
generic, and generate new functions specifically for those types

```
generic<type T>
print<T>(t T);

//////////////

print_string(t string);
print_int32(t int32);
print_vector3(t vector3);
```

#### Macros

Replace the relevent blocks with the provided substitution.

#### Mixins

All members get copied into the struct. Name clashes resolved with an
added prefix. If a name clash occurs and function name is called normally
replace the call with the call to the higher priority function.

For casting the mixin's name becomes a function. Each struct then gets
their own version of the function that returns an object with the necessary
pointers.

#### Anonymous Structs

Get given a name that is generated from the file, namespace and an ID,
then get moved to be regular structs.

#### Separate Declarations and Defintions

The definition gets moved to the declaration.

#### Inferred Types

Get replaced with the explicit type.

#### Operators

Get replaced with their backing function.

#### Compound Statements

Get split into individual statements, in the order they are evaluated.

```
dot := a * b * cos(theta);

// to

temp_INT32 int32 = __multiply(a, b);
dot int32 = __multiply(temp_INT32, cos(theta));

//////////////

// Lets say its in main.eso, line 330
l2 = stream(l1).map( ... ).filter( ... ).reverse().toArray();

// to

temp_STREAM stream = stream(l1);                /// main.eso:330
temp_STREAM stream = temp_STREAM.map( ... );    /// main.eso:330
temp_STREAM stream = temp_STREAM.filter( ... ); /// main.eso:330
temp_STREAM stream = temp_STREAM.reverse();     /// main.eso:330
l2 = temp_STREAM.toArray();                     /// main.eso:330

```

#### Function Overloading

Each function gets given a unique identifier as a prefix. The relevant
calls are changed appropriately.

#### Default Parameters

In all places the function uses a default param, the constant gets
put in the relevant position.

#### `params` Keyword

An array gets constructed from the inputs.

#### Encapsulation

All relevant keywords get removed.
