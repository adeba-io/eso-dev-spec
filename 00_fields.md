# Fields

## Declaration and Assignment

Fields are declared in a post-type notation.

```
// No initialisation, whatever's there is there
jumpsLeft s8
greeting string

// Default initialisation, sets all member fields to their default value
jumpsLeft s8{}
greeting string{}

// Calls the default creator or if it doesn't have one the default creator of all its member fields
// If one of the member fields doesn't have one, through an error
jumpsLeft s8()
greeting string()

// Initialisation by copy assignment, if copying is not enabled throws an error
jumpsLeft s8 = 4
greeting string = 'How do you do?'
```

As this is a statically-typed language, the types of Fields can be declared at compile time. Thus, you can omit the type using the following notation.

```
jumpsLeft := 4
greeting := 'How do you do?'
```

Arrays can be declared as such.

```
// This is declared on the stack
numbers [10] s8 // If we don't assign, we must declare size

// Notice the array is declared using square brackets []
// Allocated on the stack
hello [] char = [ 'H', 'e', 'l', 'l', 'o' ]
randomNums := f32 [ 1.0, 0.4, 1., .33, 0.41, 1 ]

// Can have size declared at runtime like so
// Also allocated on the stack
numPlayers u8 = retrievePlayers()
players [numPlayers] sprite

matrix [4][4]f32
```

Arrays are not automatically cast to pointers as in C, they are instead objects that contain some useful information. Most notably the array size.

```
winnerIds := s8 [ 1211, 1432, 9854 ]
winnerIds.size // 3, Could use 'length', 'count', or 'size'
```

## mut Keyword

```
player1Score := 5
player1Score = 10 // Error
```

All the fields shown thus far are immutable. Thus, the code just above throws an error.
We can make the above valid by prefixing the field with `mut`.

```
mut player1Score s8 = 5
player1Score = 10 // OK
```

*Should all fields be immutable by default?*

## Field Pointers

Rather than containing data itself, pointers contain the memory address of data.

```
jumpsLeft *s8
greeting *string

// Assign a memory location to jumpsLeft
jumpsLeft *s8 =* malloc(#size(s8))
jumpsLeft = 4 // This writes to the memory location behind `jumpsLeft`

jumpsLeft *s8(10)
// Equivalent to: jumpsLeft *s8 =* malloc(#size(s8)); jumpsLeft.__create(10)

jumpsLeft *s8 = 4           // jumpsLeft *s8 =* malloc ...; jumpsLeft = 4

// Pointer to an array of 10 s32s
winnerIDs *[10] s32
// An array of 10 pointers to s32
winnerIds [10] *s32

// Type inferred:
jumpsLeft :*=  4     // jumpsLeft *u8 = malloc ...; jumpsLeft = 4
greeting :*= 'Hello' // greeting *[5] char = malloc(#size([5]char)); greeting = ['H', 'e', 'l', 'l', 'o']

// Free memory
free(jumpsLeft)
```

We can specify whether we want the pointer itself, the data being pointed to to be mutable or both.

```
// Neither the pointer not the data are mutable
jumpsLeft *s8

// Only the data is mutable
mut jumpsLeft *s8

// Only he pointer is mutable
mut* jumpsLeft *s8

// Both are mutable, short for 'variable'
var jumpsLeft *s8
```

Pointers can be used just as if they were regular Fields. To access and operate on the memory address the pointer, you must use the `*` operator.

```
// Needs the mut* declaration
mut* jumpsLeft *s8 = 6
jumpsLeft*  // Returns the memory location jumpsLeft points to
jumpsLeft** // Returns the memory location jumpsLeft occupies

jumpsLeft =* jumps // Gives jumpsLeft the memory location behind jumps
jumpsLeft =* 0xB30C91C
jumpsLeft =* jumps* + 5

jumpsLeft*++; // Increment by size of jumpsLeft's type
```
